function [ input_intensity ] = f_get_Gaussian(sz)
%F_GET_GAUSSIAN Summary of this function goes here
%   Detailed explanation goes here
	x = linspace(-10,10,sz(2));
	y = linspace(-10,10,sz(1));
	[X,Y] = meshgrid(x,y);
	x0 = 0;     		% center
	y0 = 0;     		% center
	sigma = 2; 			% beam waist
	A = 1;      		% peak of the beam 
	res = ((X-x0).^2 + (Y-y0).^2)./(2*sigma^2);
	input_intensity = A  * exp(-res);
	% surf(input_intensity);
	% shading interp 

end

