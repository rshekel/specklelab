close all; clear all;
%% Doc 
% We try to implement a naive feedback algorithm using phases on SLM 

%% setting HW
% Set root object a.k.a. screen to be in pixels unit 
set(0,'units','pixels');

% Set mako camera
[src, vid] = f_get_camera();
src.ExposureTime = 100000; % in microsec
% vid.ROIPosition = [352,406,16,8]; 

triggerconfig(vid,'manual')
start(vid)

% Set SLM
correction=double(imread('F:\SLM-x13138-05\deformation_correction_pattern\CAL_LSH0801946_400nm.bmp')); 
mat_size = size(correction);

slm_fig = figure(100);
f_set_slm_figure(slm_fig); 

camera_fig = figure(30);
axes(camera_fig); % Create axis 

Lambda = 404e-6; % mm = 404nm

%% Actual interesting code 
% Adding a phantom diffuser, that our algorithm will try and optimize using
% phases. obviously the optimal result will be phase = -random_diffuser 
random_diffuser = randi(256, mat_size) - 1;
correction = mod(correction + random_diffuser, 256);
phase = zeros(mat_size);
best_phase = zeros(mat_size);
src.ExposureTime = 100000; % in microsec

%% Show beginning 
stop(vid);
% The middle here is really the interesting part (cropping vid to avoid DC noise)
vid.ROIPosition = [100,100,200,200];
start(vid);
f101 = figure(101); 
axes(f101); % Create axis 
img = double(getsnapshot(vid));
imagesc(f101.CurrentAxes, img); 
title('before optimization');
colorbar(f101.CurrentAxes);

%% Optimize
stop(vid);
% We will try and maximize intensity on this region 
vid.ROIPosition = [190,190,20,20];
start(vid);

iterations = 500;
max_power = 0;

for iter = 1:iterations 
    % Pick random half of pixels to play with
    pixels_to_change = randi(2, mat_size) -1;

    last_best_phase = best_phase;
    N = 7; 
    phis = linspace(0.1, 2*pi, N);
    Is = zeros(1,N);
    for j = 1:N 
        phase = last_best_phase + phis(j)*pixels_to_change;
        f_put_phase_on_SLM(slm_fig, phase, correction) % applying the mask
        pause(0.2) % Wait for the phase on the SLM to stabilize. 
        img = double(getsnapshot(vid));
        power = sum(sum(img(9:11, 9:11)));
        Is(j) = power;
        
        %{
        if power > max_power
            max_power = power;
            best_phase = phase;
            imagesc(camera_fig.CurrentAxes, img); 
            colorbar(camera_fig.CurrentAxes);
            fprintf('iteration: %d, max_power: %d\n', i, max_power);
        end
        %}
    end
    C = sum(Is.*cos(phis));
    S = sum(Is.*sin(phis));
    A = C + 1i*S;
    phi_0 = angle(A);
    
    % Check best_phase
    f_put_phase_on_SLM(slm_fig, phase, correction) % applying the mask
    pause(0.2) % Wait for the phase on the SLM to stabilize. 
    img = double(getsnapshot(vid));
    imagesc(camera_fig.CurrentAxes, img); 
    colorbar;
    power = sum(sum(img(9:11, 9:11)));
    fprintf('iteration: %d, max_power: %d\n', iter, power); 
    
    if power < max(Is)
        [M, I] = max(Is);
        phi_0 = phis(I);
    end
    best_phase = last_best_phase + phi_0*pixels_to_change;

end

%% Show End
stop(vid);
% The middle here is really the interesting part (cropping vid to avoid DC noise)
vid.ROIPosition = [100,100,200,200];
start(vid);
f102 = figure(102); 
axes(f102); % Create axis 
img = double(getsnapshot(vid));
imagesc(f102.CurrentAxes, img); 
title('after optimization');
colorbar(f102.CurrentAxes);


%% closing HW
stop(vid)