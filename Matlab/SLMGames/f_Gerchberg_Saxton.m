%% DOC 
% Based on https://www.mathworks.com/matlabcentral/fileexchange/65979-gerchberg-saxton-algorithm
% By Musa AYDIN

function [phase_mask] = f_Gerchberg_Saxton(input_intensity, path, iterations)
% input_intensity of beam at SLM (Gausian? Plane wave?) and path to png
% file with picture you want to optimize to 
% Pseudo Code 
%  A = IFT(Target)
%  while error criterion is not satisfied
%    B = Amplitude(Source) * exp(i*Phase(A))
%    C = FT(B)
%    D = Amplitude(Target) * exp(i*Phase(C))
%    A = IFT(D)
%  end while
%  Retrieved_Phase = Phase(A

	Target=rgb2gray(imread(path));
	Target=double(Target);
    Target = 256 - Target;
    Target = resizem(Target, size(input_intensity));
	A = fftshift(ifft2(fftshift(Target)));
	error = [];

    for i=1:iterations
      B = abs(input_intensity) .* exp(1i*angle(A));
      C = fftshift(fft2(fftshift(B)));
      D = abs(Target) .* exp(1i*angle(C));
      A = fftshift(ifft2(fftshift(D)));
      % error = [error; sum(sum(abs(1.32*abs(C) - abs(Target))))];   
    end
    phase_mask = angle(A);
    %{
    figure;
	subplot(2,1,1);
	imshow(Target);
	title('Original image')
	subplot(2,1,2);
	imagesc(abs(C))               %last pattern
	title('reconstructed image');
	figure
	i = 1:1:i;
	plot(i,(error'));
	title('Error');
	figure
	imagesc(abs(C)) %last pattern
	title('reconstructed image');
	%} 
end