%% Doc 
% Here we show in a numeric way that the linear phase wre put on SLM works
% as a mirror. We see that the angle is very precise, but with a displacement 
% from the meant angle. We do not have a satisfactory explanation for this.

%% setting HW
% Set root object a.k.a. screen to be in pixels unit 
set(0,'units','pixels');

% Set mako camera
[src, vid] = f_get_camera();
src.ExposureTime = 1000; % in microsec
% vid.ROIPosition = [352,406,16,8]; 

triggerconfig(vid,'manual')
start(vid)

% Set SLM
correction=double(imread('F:\SLM-x13138-05\deformation_correction_pattern\CAL_LSH0801946_400nm.bmp')); 
mat_size = size(correction);

slm_fig = figure(100);
f_set_slm_figure(slm_fig); 

camera_fig = figure(30);
axes(camera_fig); % Create axis 

Lambda = 404e-6; % mm = 404nm

%% Mirror code 
src.ExposureTime = 50;
angle = deg2rad(45); 
xs = [];
ys = [];
for m = 0:20:200
    x = linspace(0, m*pi, mat_size(2));
    y = linspace(0, m*pi, mat_size(1));
    [X, Y] = meshgrid(x,y); 
    phase = sin(angle)*X + cos(angle)*Y; 

    f_put_phase_on_SLM(slm_fig, phase, correction) % applying the mask
    pause(0.5) % Wait for the phase on the SLM to stabilize. 
    img = double(getsnapshot(vid));
    [x_max,y_max] = find(img == max(img(:)));
    x_max = x_max(1); % find can return mmultiple instances of indexes 
    y_max = y_max(1); 
    xs = [xs, x_max];
    ys = [ys, y_max];     
    imagesc(camera_fig.CurrentAxes, img); 
end

figure;
title('location of max intensity');
plot(xs, ys);
hold on;
c = polyfit(xs, ys, 1);
fprintf('angle supposed to be: %d\n', rad2deg(angle));
yy = polyval(c, xs);
plot(xs, yy);
yyy = c(2) + tan(angle)*xs;
plot(xs, yyy - yyy(end) + yy(end));
legend('data', 'fit', 'expected');
fprintf('angle really is approx. %d\n', rad2deg(atan(c(1))));

%% closing HW
stop(vid)