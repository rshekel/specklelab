function [src, vid] = f_get_camera()
%Use this function at the beggining of the code to initate comunication
%with a mako camera and set her parameters

objects=imaqfind;delete(objects)

% Our camera is the Mako U-051B
% We reached these parameters and functions using the imaqtool
vid = videoinput('gentl', 3, 'Mono8');
src = getselectedsource(vid);
vid.ReturnedColorSpace='grayscale';
end
