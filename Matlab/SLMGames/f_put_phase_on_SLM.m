function [  ] = f_put_phase_on_SLM(h, phase,correction)
% h is handle to figure 

%% Importing the right correction patterns and setting the right alphas
alpha=215; %for 404nm, taken from the data sheet @@@
wavelength = 404;         % nm @@@
k = 10^9*2*pi/wavelength; % m

%% Calculating image to send to the SLM (see data sheet for an example)
phase = mod(phase*255/(2*pi)+correction,256);
phase = phase*alpha/255;
img = uint8(phase);
image(h.CurrentAxes, img);

drawnow;
end
