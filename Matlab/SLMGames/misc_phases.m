%% Doc 
% The goal of this script is to demostrate putting different phase masks on
% ths SLM and see the effect on the camera. We see 4 different effects:
% 1) linear phase creates a mirror like effect, where we can choose the
% angle. 
% 2) a ~r^2 like phase gives the effect of a lense. 
% 3) a random kind of pattern will give us speckles
% 4) Using the Gerchberg Saxton algorithm we can get any picture we want on the camera 

%% setting HW
% Set root object a.k.a. screen to be in pixels unit 
set(0,'units','pixels');

% Set mako camera
[src, vid] = f_get_camera();
src.ExposureTime = 1000; % in microsec
% vid.ROIPosition = [352,406,16,8]; 

triggerconfig(vid,'manual')
start(vid)

% Set SLM
correction=double(imread('F:\SLM-x13138-05\deformation_correction_pattern\CAL_LSH0801946_400nm.bmp')); 
mat_size = size(correction);

slm_fig = figure(100);
f_set_slm_figure(slm_fig); 

camera_fig = figure(30);
axes(camera_fig); % Create axis 

Lambda = 404e-6; % mm = 404nm

%% Actual phase code 
option = 5; 
% 1 : mirror
% 2 : lense
% 3 : speckle 
% 3.5 : speckle in better method 
% 4 : Gerchberg Saxton Algorithm - drawings! 
% 5 : slits 

if option == 1 % Mirror  
    src.ExposureTime = 100;
    angle = -pi*2/6; 
    for m = 0:10:100
        x = linspace(0, m*pi, mat_size(2));
        y = linspace(0, m*pi, mat_size(1));
        [X, Y] = meshgrid(x,y); 
        phase = sin(angle)*X + cos(angle)*Y; 

        f_put_phase_on_SLM(slm_fig, phase, correction) % applying the mask
        pause(0.5) % Wait for the phase on the SLM to stabilize. 
        img = double(getsnapshot(vid));
        imagesc(camera_fig.CurrentAxes, img); 
    end

elseif option == 2 % Lense 
    % https://en.wikipedia.org/wiki/Thin_lens#Physical_optics
    % in physical optics the phase a lense gives to a beam is given by
    % exp(2*pi*1j/lambda * r^2/2f) 
    
    src.ExposureTime = 10000; % in microsec
    pix_to_mm = 12.5e-3; % 1 pixel of SLM == 12.5 um == 12.5e-3 mm
    % try using f = 50, 100, 150
    for f = 100 % mm. You can see effect with finding focus with card between SLM and lense. 
        fprintf('f=%d\n', f);
        
        % The minus sign is important and makes it converging and not
        % diverging 
        K = -(pi / (Lambda*f)) * pix_to_mm^2;

        rad_amount = max(mat_size)/1.5;
        % phase_range = linspace(0, max_phase, rad_amount); 

        phase = zeros(mat_size); 
        origin = [round(mat_size(2)/2) round(mat_size(1)/2)];
        [xx,yy] = meshgrid((1:mat_size(2))-origin(1),(1:mat_size(1))-origin(2)); % create x and y grid. 

        zz = round(sqrt(xx.^2 + yy.^2)); % each element in matrix has its radius 
        for radius = 1:2:rad_amount
            phase(zz == radius) = K*radius^2; 
        end
        f_put_phase_on_SLM(slm_fig, phase, correction) % applying the mask
        pause(0.5) % Wait for the phase on the SLM to stabilize. 
        img = double(getsnapshot(vid));
        imagesc(camera_fig.CurrentAxes, img); 

        % figure;
        % imagesc(phase); 
    end
    
elseif option == 3 % Speckles 
    
    for block_size = 2:2:40
        phase = 2*pi*rand(size(correction));
        block_size_x = block_size; 
        block_size_y = block_size; 
        
        % This logic is to care care of dimension mismatch, when
        % 1272%block_size != 1272%3
        % Instead of all this stupid logic I probably should have used
        % resizem. See option 3.5 
        for i=1:block_size_y-1
            if numel(phase(:, i:block_size_y:end)) == numel(phase(:, block_size_y:block_size_y:end))
                phase(:, i:block_size_y:end) = phase(:, block_size_y:block_size_y:end);
            else
                phase(:, i:block_size_y:end) = [phase(:, block_size_y:block_size_y:end), phase(:, 1)];
            end
        end
        for i=1:block_size_x-1
            if numel(phase(i:block_size_x:end, :)) == numel(phase(block_size_x:block_size_x:end, :))
                phase(i:block_size_x:end, :) = phase(block_size_x:block_size_x:end, :);
            else
                phase(i:block_size_x:end, :) = [phase(block_size_x:block_size_x:end, :); phase(1, :)];
            end        
        end

        f_put_phase_on_SLM(slm_fig, phase, correction) % applying the mask
        pause(0.5) % Wait for the phase on the SLM to stabilize. 
        img = double(getsnapshot(vid));
        imagesc(camera_fig.CurrentAxes, img); 
    end
    
elseif option == 3.5 % Speckle in not stupid way
    for pixels_in_row = 2:4:80
        phase = 2*pi*rand([pixels_in_row, pixels_in_row]);
        phase = resizem(phase, mat_size);

        f_put_phase_on_SLM(slm_fig, phase, correction) % applying the mask
        pause(0.5) % Wait for the phase on the SLM to stabilize. 
        img = double(getsnapshot(vid));
        imagesc(camera_fig.CurrentAxes, img); 
    end
    
elseif option == 4 % Gerchberg Saxton 
    % Following: 
    % https://en.wikipedia.org/wiki/Gerchberg%E2%80%93Saxton_algorithm 
    % https://www.mathworks.com/matlabcentral/fileexchange/65979-gerchberg-saxton-algorithm
    
    src.ExposureTime = 100000; % in microsec
    
    % Interesting that it doesn't really matter if we put plane wave or
    % gaussian. It has to do with the fact that GS works after 1 iteration
    % pretty well - and that dowsn't care about input_intensity. 
    input_intensity = f_get_Gaussian(mat_size);
    input_intensity = ones(mat_size);
    path = 'Drawings/RLB.png';
    iterations = 10;

    phase = f_Gerchberg_Saxton(input_intensity, path, iterations);
    
    f_put_phase_on_SLM(slm_fig, phase, correction) % applying the mask
    pause(0.5) % Wait for the phase on the SLM to stabilize. 
    img = double(getsnapshot(vid));
    imagesc(camera_fig.CurrentAxes, img); 
    set(camera_fig.CurrentAxes,'YDir','normal');
    set(camera_fig.CurrentAxes,'XDir','reverse'); 
        
elseif option == 5 % Slits 
    src.ExposureTime = 1000000;
    % Phase of 460*pi is enough to get the main beam out of camera on X
    % Axis
    x = linspace(0, 460*pi, mat_size(2));
    y = linspace(0, 460*pi, mat_size(1));
    [X, Y] = meshgrid(x,y); 
    phase = -X;
    
    % Slits to have effect on Y-axis to differentiate from secondary angle
    % effects 
    % Single slit - should have a sinc(Bx) function, where the wider the
    % slit the bigger B and the more peaks we should see
    for i = 2:2:20
        phase(500:500+i, :) = 0;
        % phase(540:540+i, :) = 0;

        f_put_phase_on_SLM(slm_fig, phase, correction) % applying the mask
        pause(0.5) % Wait for the phase on the SLM to stabilize. 
        img = double(getsnapshot(vid));
        imagesc(camera_fig.CurrentAxes, img); 
        
        figure; 
        vec = img(:, 566);
        plot(vec);
    end 

end

%% closing HW
stop(vid)