%% Doc 
% Here we show in a numeric way The dependance of the speckle pattern on
% the pixel size we put on SLM. 

%% setting HW
% Set root object a.k.a. screen to be in pixels unit 
set(0,'units','pixels');

% Set mako camera
[src, vid] = f_get_camera();
src.ExposureTime = 1000; % in microsec
% vid.ROIPosition = [352,406,16,8]; 

triggerconfig(vid,'manual')

start(vid)

% Set SLM
correction=double(imread('F:\SLM-x13138-05\deformation_correction_pattern\CAL_LSH0801946_400nm.bmp')); 
mat_size = size(correction);

slm_fig = figure(100);
f_set_slm_figure(slm_fig); 

camera_fig = figure(30);
axes(camera_fig); % Create axis 

Lambda = 404e-6; % mm = 404nm

%% Actual phase code 
% Pixel size on Camera is ~5um. 
% Option 1 is showing NA of speckles dependance on pixel size on SLM
% Option 2 is showing probability density of power with Prob(I) = exp(-I/I0) 
% Option 3 is showing speckle size using fourier transform (not really
% working)
% Option 4 is showing speckle size using autocorrelation 

option = 1;

if option == 1 % Show NA of speckles dependance on pixel size on SLM
    for pixels_in_row = 40:20:200
        fprintf('pixels in row: %d\n', pixels_in_row);
        sum_img = zeros([600, 800]);
        for i = 1:15
            phase = 2*pi*rand([pixels_in_row, round(pixels_in_row*1.3)]);
            phase = resizem(phase, mat_size);

            f_put_phase_on_SLM(slm_fig, phase, correction) % applying the mask
            pause(0.5) % Wait for the phase on the SLM to stabilize. 
            img = double(getsnapshot(vid));
            imagesc(camera_fig.CurrentAxes, img); 
            sum_img = sum_img + img;
        end
        figure;
        threshold = 2.5*mean(mean(sum_img));
        imagesc(sum_img); 
        colorbar;

        A = sum_img >= threshold;
        figure; imagesc(A);
        [centers, radii, metric] = imfindcircles(A,[50 2000], 'EdgeThreshold', 0.05, 'Sensitivity', 0.95);    
        viscircles(centers, radii,'EdgeColor','b');
        fprintf('Radius: %d\n', radii);
        % area = sum(sum(sum_img>=threshold));
    end
    
elseif option == 2 % Show Prob(I) is exp(-I/I0) 
    src.ExposureTime = 7500; % in microsec
    stop(vid);
    vid.ROIPosition = [500 230 140 140];
    start(vid);
    
    for pixels_in_row = 40:10:100

        phase = 2*pi*rand([pixels_in_row, round(pixels_in_row*1.3)]);
        phase = resizem(phase, mat_size);
        f_put_phase_on_SLM(slm_fig, phase, correction) % applying the mask
        pause(0.5) % Wait for the phase on the SLM to stabilize. 
        img = double(getsnapshot(vid));
        imagesc(camera_fig.CurrentAxes, img); 
        colorbar;
        figure;
        hist(img(:), 128);
        xlim([0 100]);
        ylim([0 4500]);
    end
    
elseif option == 3 
    src.ExposureTime = 7500; % in microsec
    stop(vid);
    % vid.ROIPosition = [0 230 140 140];
    start(vid);
    pixels_in_row = 300;
    phase = 2*pi*rand([pixels_in_row, round(pixels_in_row*1.3)]);
    phase = resizem(phase, mat_size);
    f_put_phase_on_SLM(slm_fig, phase, correction) % applying the mask
    pause(0.5) % Wait for the phase on the SLM to stabilize. 
    img = double(getsnapshot(vid));
    imagesc(camera_fig.CurrentAxes, img); 
    colorbar;

    A = fftshift(fft2(fftshift(img)));
    
    Fs_x = 1e3;           % pixel per meter length
    N_x = 140;            % pixels
    dF_x = Fs_x/N_x;      % 1/meter
    f_x = -Fs_x/2:dF_x:Fs_x/2-dF_x + (dF_x/2)*mod(N_x,2);
    Fs_y = 1e3;           % pixel per meter length
    N_y = 140;            % pixels
    dF_y = Fs_y/N_y;      % 1/meter
    f_y = -Fs_y/2:dF_y:Fs_y/2-dF_y + (dF_y/2)*mod(N_y,2);
    
    [freq_x, freq_y] = meshgrid(f_x, f_y);
    
    B = abs(A);
    B(B > 3e5) = 0;
    figure;
    imagesc(flipud(B), 'XData', [min(f_x) max(f_x)], 'YData', [min(f_y) max(f_y)]);
    colorbar;
    
elseif option == 4 
    disp('maybe one day...');
end    

%% closing HW
stop(vid)