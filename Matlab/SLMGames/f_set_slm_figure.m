function [] = f_set_slm_figure( fig_handle )
%F_SET_SLM_FIGURE Set Position, grayscale, no axes etc. for SLM figure
%   Detailed explanation goes here
h = fig_handle;
colormap(h, gray(256))
slm_size = [1024,1280];

% Here SLM screen is on left, and main screen has 1920 pixels 
scrsz=[1920+5 176 slm_size(2) slm_size(1)]; 
set(h,'Position',scrsz);
set(h,'menubar','none','toolbar','none')

% Set colormap and get rid of figure axes
set(h.CurrentAxes,'xtick',[],'ytick',[]);
axis image
set(h.CurrentAxes,'units','pixels');
set(h,'units','pixels');
set(h.CurrentAxes,'visible','off');
set(h,'Position',scrsz)
set(h.CurrentAxes,'position',[1 1 (scrsz(3)) scrsz(4)]);
set(h.CurrentAxes,'xtick',[],'ytick',[]);
end
